package encryption

import (
	"bitbucket.org/antondom/evchargerlibrary/config"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

type TokenInfo struct {
	Id      int    `json:"id"`
	Login   string `json:"login"`
	Email   string `json:"email"`
	Role    string `json:"role"`
	Company string `json:"company"`
	jwt.StandardClaims
}

func GetHash(password string) string {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		panic(err)
	}
	return string(hashedPassword)
}

func GenerateAuthToken(userId int, login string, email string, role string, company string) string {
	fmt.Println(userId)
	fmt.Println(login)
	cl := TokenInfo{
		userId,
		login,
		email,
		role,
		company,
		jwt.StandardClaims{},
	}
	cl.StandardClaims.ExpiresAt = time.Now().Add(time.Minute * time.Duration(config.AuthTokenMin)).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, cl)
	fmt.Println(token)
	signedToken, _ := token.SignedString([]byte(config.SecretToken))
	fmt.Println(signedToken)
	return signedToken
}

func GenerateRefreshToken(userId int) string {
	cl := TokenInfo{}
	cl.Id = userId
	cl.StandardClaims.ExpiresAt = time.Now().Add(time.Minute * time.Duration(config.RefreshTokenMin)).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, cl)
	signedToken, _ := token.SignedString([]byte(config.SecretToken))
	fmt.Println(signedToken)
	return signedToken
}

func GeneratePassword(strlen int) string {
	const chars = "abcdefghijklmnopqrstuvwxyz0123456789"
	rand.Seed(time.Now().UTC().UnixNano())
	result := make([]byte, strlen)
	for i := range result {
		result[i] = chars[rand.Intn(len(chars))]
	}
	return string(result)
}

func GetUserParams(c *gin.Context) TokenInfo {
	tok, _ := c.Get("TOK")
	var requestInfo TokenInfo
	json.Unmarshal([]byte(tok.(string)), &requestInfo)
	return requestInfo
}

func GetDate(str string) time.Time {
	fmt.Println("mmm")
	date, err := time.Parse("2006-01-02 15:04:05", str)
	if err != nil {
		fmt.Println(err)
		date, err = time.Parse("15:04:05 2006-01-02", str)
		return date
	} else {
		fmt.Println("ok")
		return date
	}
}

func Authorize(c *gin.Context) {
	token := c.GetHeader("Authorization")
	if len(token) == 0 {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	err := checkToken(token, c)

	if err != nil {
		if err.Error() == "Token is expired" {
			c.AbortWithStatus(http.StatusPaymentRequired)
		} else {
			c.AbortWithStatus(http.StatusUnauthorized)
		}
	} else {
		c.Next()
	}
}

func checkToken(stringToken string, c *gin.Context) error {
	if strings.Contains(stringToken, "JWT ") {
		stringToken = strings.Trim(stringToken, "JWT ")
	}
	token, err := jwt.Parse(stringToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.SecretToken), nil
	})

	if err != nil {
		fmt.Println(err)
		return err
	}

	if !token.Valid {
		fmt.Println("token no valid")
		return err
	} else {
		ff, _ := json.Marshal(token.Claims.(jwt.MapClaims))
		c.Set("TOK", string(ff))
		return nil
	}
}
