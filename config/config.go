package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
)

var SecretToken string
var DbUser string
var DbPassword string
var DbHost string
var DbName string
var AuthPort string
var AvatarPath string
var StationImagePath string
var RefreshTokenMin int
var AuthTokenMin int
var StationPort string
var StationImageUrl string
var AvatarUrl string
var UrlOcppJ string
var UrlOcppS string
var UrlOurProtocol string
var PaymentPort string
var EventsPort string
var LogServer string
var LogPort string
var LogSavePath string
var LogServerPath string
var ReportPath string
var ReportUrl string
var UpdateUrl string
var UpdatePath string
var PaymentService string
var SSlKey string
var SSLCertificate string

func init() {

	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	dir := filepath.Dir(ex)
	dir = dir + "/config.json"
	fmt.Println(dir)
	plan, err := ioutil.ReadFile(dir)
	if err != nil {
		panic("config file not found")
	}

	var result map[string]string
	json.Unmarshal(plan, &result)
	SecretToken = result["SecretToken"]
	DbUser = result["DbUser"]
	DbPassword = result["DbPassword"]
	DbHost = result["DbHost"]
	DbName = result["DbName"]
	AuthPort = result["AuthPort"]
	AvatarPath = result["AvatarPath"]
	StationImagePath = result["StationImagePath"]
	RefreshTokenMin, err = strconv.Atoi(result["RefreshTokenMin"])
	AuthTokenMin, err = strconv.Atoi(result["AuthTokenMin"])
	StationPort = result["StationPort"]
	StationImageUrl = result["StationImageUrl"]
	AvatarUrl = result["AvatarUrl"]
	UrlOcppJ = result["UrlOcppJ"]
	UrlOcppS = result["UrlOcppS"]
	UrlOurProtocol = result["UrlOurProtocol"]
	PaymentPort = result["PaymentPort"]
	EventsPort = result["EventsPort"]
	LogPort = result["LogPort"]
	LogServer = result["LogServer"]
	LogSavePath = result["LogSavePath"]
	LogServerPath = result["LogServerPath"]
	ReportPath = result["ReportPath"]
	ReportUrl = result["ReportUrl"]
	UpdateUrl = result["UpdateUrl"]
	UpdatePath = result["UpdatePath"]
	PaymentService = result["PaymentService"]
	SSlKey = result["SSlKey"]
	SSLCertificate = result["SSLCertificate"]

	if _, err := os.Stat(StationImagePath); os.IsNotExist(err) {
		err = os.MkdirAll(StationImagePath, 0755)
		if err != nil {
			panic(err)
		}
	}

	if _, err := os.Stat(AvatarPath); os.IsNotExist(err) {
		err = os.MkdirAll(AvatarPath, 0755)
		if err != nil {
			panic(err)
		}
	}

	if _, err := os.Stat(LogSavePath); os.IsNotExist(err) {
		err = os.MkdirAll(LogSavePath, 0755)
		if err != nil {
			panic(err)
		}
	}

	if _, err := os.Stat(ReportPath); os.IsNotExist(err) {
		err = os.MkdirAll(ReportPath, 0755)
		if err != nil {
			panic(err)
		}
	}

}
