package db

import (
	"bitbucket.org/antondom/evchargerlibrary/config"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"sync"
)

var instanceDatabaseConnection *databaseConnection
var onceDatabaseConnection sync.Once

type databaseConnection struct {
	Connection *sqlx.DB
}

func init() {
	fmt.Println("db init")
	instanceDatabaseConnection = newDBConnection()
}

func newDBConnection() *databaseConnection {
	var err error
	createdDatabaseConnection := &databaseConnection{}
	dataSource := config.DbUser + ":" + config.DbPassword + "@tcp(" + config.DbHost + ")" + "/" + config.DbName
	createdDatabaseConnection.Connection, err = sqlx.Open("mysql", dataSource)
	CheckError(err)
	return createdDatabaseConnection
}

func GetDBConnect() *sqlx.DB {
	return instanceDatabaseConnection.Connection
}

func CheckError(err error) {
	if err != nil && err.Error() != "sql: no rows in result set" {
		fmt.Println(err)
		panic(err)
	}
}
